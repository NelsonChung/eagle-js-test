import Vue from 'vue'
import App from './App.vue'
import Eagle from 'eagle.js'
// import animate.css for slide transition 
import 'animate.css'

Vue.config.productionTip = false

Vue.use(Eagle)

new Vue({
  render: h => h(App),
}).$mount('#app')
